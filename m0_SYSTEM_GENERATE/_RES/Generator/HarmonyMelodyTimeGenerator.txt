﻿"STORE"
	import @System\Lib\Music direct meta
	import @System\Lib\Std direct meta
	class "HarmonyMelodyTimeGenerator"
		attribute "BaseOctave" @Integer
			default "3"
			<@$Section :: "Generator">
		association "MelodyFlow" @Generator\MelodyFlow "0":"1"
			<@$Section :: "Input">
		association "TriggerSet" @Generator\TriggerSet "0":"1"
			<@$Section :: "Input">
		association "ChordProgression" @Generator\ChordProgression "0":"1"
			<@$Section :: "Input">
		association "Output" @Sequence "0":"1"
			<@$Section :: "Output">
		method "CopyAtomAttributes" ()
			<@System\Meta\Presentation\$Hide :: @$Empty>
			if Output\Length === ~00
				Output +< @@HasLength\Length :: _"0"
			Output\Length = _TriggerSet\Length
			if Output\IsDrum === ~00
				Output +< @@Sequence\IsDrum :: _"False"
			if MelodyFlow\IsDrum<> == "1"
				Output\IsDrum = _MelodyFlow\IsDrum
		method "CreateEventsNoDrum" ()
			<@System\Meta\Presentation\$Hide :: @$Empty>
			variable "note" @Integer
			variable "counter" @Integer
			variable "melody" @Integer
			variable "sortedChordProgressionChord1" @Vertex
			sortedChordProgressionChord1 = @@NumericSortByQuery[ChordProgression\Chord<<"1">>\Pitch, "Note:"]
			Output -< Output\Event:
			counter = "1"
			foreach t in TriggerSet\Trigger:
				Output +< @@History\Event :: t{
					melody = MelodyFlow\Step<<counter>>\Quant\Note
					note = _sortedChordProgressionChord1<<melody + "1">>\Note
					@@$Is ;; @@NoteEvent
					@@Event\TriggerTime :: _t\TriggerTime
					@@HasLength\Length :: _t\Length
					@@Pitch\Octave :: _BaseOctave
					@@Pitch\Note :: note
					@@Note\Velocity :: _t\Velocity
					counter = counter + "1"
				}
		method "CreateEventsDrum" ()
			<@System\Meta\Presentation\$Hide :: @$Empty>
			variable "counter" @Integer
			Output -< Output\Event:
			counter = "1"
			foreach t in TriggerSet\Trigger:
				foreach quant in MelodyFlow\Step<<counter>>\Quant
					Output +< @@History\Event :: t{
						@@$Is ;; @@NoteEvent
						@@Event\TriggerTime :: _t\TriggerTime
						@@HasLength\Length :: - "0"
						@@Pitch\Octave :: _quant\Octave
						@@Pitch\Note :: _quant\Note
						@@Note\Velocity :: _t\Velocity
					}
				counter = counter + "1"
		method "PutIntoStore" ()
			<@System\Meta\Presentation\$Hide :: @$Empty>
			variable "parentVertex" @Vertex
			parentVertex = Output#Sequence:
			parentVertex <+< @@Sequence ;; Output
			parentVertex -< @@Sequence ;; this\Output
		method "Dispose" ()
			<@System\Meta\Presentation\$Hide :: @$Empty>
		method "Process" ()
			variable "IsDrum" @Boolean
			IsDrum = "False"
			this.CopyAtomAttributes[]
			if TriggerSet\IsDrum<> == "1"
				if TriggerSet\IsDrum == "True"
					IsDrum = "True"
			if !IsDrum
				this.CreateEventsNoDrum[]
			if IsDrum == "True"
				this.CreateEventsDrum[]
			this.PutIntoStore[]
			this.Dispose[]